import { Component, OnInit } from '@angular/core';
import { AlphaVantageService } from './alpha-vantage.service';

@Component({
  selector: 'app-acciones',
  templateUrl: './acciones.component.html',
  styleUrls: ['./acciones.component.css']
})
export class AccionesComponent implements OnInit {
  stockData: any;
  inputSymbol: string = ''; // Agrega esta línea para definir inputSymbol como propiedad de la clase

  constructor(private alphaVantageService: AlphaVantageService) { }

  ngOnInit(): void {
    // Comenta o elimina esta línea si deseas que el usuario busque un símbolo antes de mostrar los datos
    // this.fetchStockData();
  }

  async fetchStockData() {
    try {
      const symbol = this.inputSymbol;
      const interval = '60min';
      const apiKey = 'B655XYS0VQVY6HXV';

      const data = await this.alphaVantageService.getTimeSeriesIntraday(symbol, interval, apiKey);
      this.stockData = data;
    } catch (error) {
      console.error('Error fetching stock data:', error);
    }
  }

  getTimeSeriesKeys(timeSeries: object): string[] {
    return Object.keys(timeSeries);
  }
}
