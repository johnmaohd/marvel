import { Injectable } from '@angular/core';
import axios from 'axios';


@Injectable({
  providedIn: 'root'
})
export class AlphaVantageService {
  private baseUrl = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=IBM&interval=5min&apikey=B655XYS0VQVY6HXV';

  constructor() { }

  async getTimeSeriesIntraday(
    symbol: string,
    interval: string,
    apiKey: string,
    outputsize?: string,
    adjusted?: boolean,
    datatype?: string
  ): Promise<any> {
    const params = {
      function: 'TIME_SERIES_INTRADAY',
      symbol,
      interval,
      apikey: apiKey,
      outputsize: outputsize || 'compact',
      adjusted: adjusted || true,
      datatype: datatype || 'json',
    };

    try {
      const response = await axios.get(this.baseUrl, { params });
      return response.data;
    } catch (error) {
      console.error('Error fetching data:', error);
      throw error;
    }
  }
}




